HLC Cluster Salt Data
=====================

This git repo is the /srv/salt directory maintained for the HLC Cluster
Salt Master. It is intended to be run either on my workstation running a
standalone Salt Master container (while the cluster is being built)
or to be used as part of an automated Salt Master Kubernetes deployment.

Nodegroup Info
--------------

This repo is designed to work with two Nodegroups:

- hlc-master - Kubernetes Master Nodes
- hlc-worker - Kubernetes Worker Nodes

Here is an sample nodegroups.conf file that can be added to Salt's
master.d directory:

```
nodegroups:
  hlc-master:
    - hlc01.hlc.eagleworld.net
    - hlc02.hlc.eagleworld.net
  hlc-worker:
    - hlc03.hlc.eagleworld.net
    - hlc04.hlc.eagleworld.net
    - hlc05.hlc.eagleworld.net
    - hlc06.hlc.eagleworld.net
```
