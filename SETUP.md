Happy Little Cloud Kubernetes Cluster Setup Guide
=================================================

This file documents the full setup of the HLC Cluster, based on the most
current state of the deployment process. The goal is to have as much of
the process as automated as possible, so manual intervention is kept to
a minimum.

Cluster Requirements
--------------------

Here are the physical requirements for making this cluster:

- 6 Raspberry Pi 3 B+ computers
- 6 Class-10 64GB MicroSD cards
- 1 8-port USB power supply
- 1 USB-powered 8-port unmanaged switch
- 1 USB-powered travel WiFi router (OpenWRT is preferred)
- 8 MicroUSB cables
- 8 Ethernet cables (7 for the cluster, 1 to connect your workstation)

It is also suggested to get a Raspberry Pi cluster case. A simple
dogbone-style 6-tier case will work fine. Additional tiers could be
used to house the additional hardware or for additonal Pis.

Workstation
-----------

To support the setting up the cluster, a workstation will need to be
used to support the cluster before it is self-reliant. The system will
need an SD card slot and will need to have Docker installed. This guide
was written to be used with a Linux-based system, but any system will
work...just substitute the commands and have your favorite disk formatting
utility available to set up the SD cards.

Note that this setup is designed to never logging on to the Pis directly,
so no additional hardware is required

Physical Setup
--------------

Once all the hardware is plugged in and set up, only one cable needs to
be handled on a regular basis: the power cable. In addition, the router
can be hard-wired to the network if better network performance is
needed based on the usage of the cluster.

Note that the Raspberry Pis should not be plugged in until the SD cards
are formatted and installed.

Network Setup
-------------

The router does not need a lot of configuration, but for the cluster to
be portable, an easy-to-configure router is recommended. I am using a
GL.iNet brand router which has a wizard to quickly connect the router
to any network either using either WiFi or Ethernet. The router is also
running OpenWRT, giving me advanced functionality when needed.

Plug in your workstation to the switch and set up the network as follows:
- Network: 10.31.1.0/24
- Gateway: 10.31.1.1

In addition, the wireless network should be configured as well to allow
for easier administration of the cluster.

Formatting the SD Cards
-----------------------

NOTE: This guide was written using the 2018-11-13 version of Raspbian Lite,
      so the commands below assume that version.

Grab the latest Raspbian Lite image at the following URL:
https://downloads.raspberrypi.org/raspbian_lite_latest

Open a shell and unzip the image:
$ cd /path/to/folder
$ unzip 2018-11-13-raspbian-stretch-lite.zip

Use the lsblk command to determine what your SD card device is. The
device on my system is /dev/mmcblk0.

Format the card with the image and use fdisk to modify the disk:
$ sudo dd bs=4M if=2018-11-13-raspbian-stretch-lite.img \
  of=/dev/mmcblk0 conv=fsync status=progress
$ sudo fdisk /dev/mmcblk0

The card will have 2 partitions created from the image. Take note of what
block the second partition starts on. Delete the second partition and 
recreate it starting at the same block and with a size of 10GB (+10G).
When prompted to delete the existing partition data, select no. Finally,
create the third partition with the ext4 format, using up the remainder
of the disk space. Write the changes and quit fdisk.

Next, to enable ssh on the system, a file needs to be added to the first
filesystem. Run the following to do so:
$ sudo mount /dev/mmcblk0p1 /mnt/usb/
$ sudo touch /mnt/usb/ssh
$ sudo umount /mnt/usb

Repeat the above steps for all 6 cards.

Setting up Static DHCP Leases
-----------------------------

First, find your workstation's MAC address by checking the active
connections on the router. Set that MAC address to the IP 10.31.1.100.

Next, plug in the first Raspberry Pi and let it start up. Once it's on
the network, set up its MAC Address to 10.31.1.11. Repeat with the
remaining Raspberry Pis based on the hosts file below.

Logging in to the Raspberry Pis
-------------------------------

Before logging in, edit your workstation's /etc/hosts file to include
the following:
  
'''
10.31.1.1   hlr.hlc.eagleworld.net hlr.hlc      hlr
10.31.1.11  hlc01.hlc.eagleworld.net hlc01.hlc  hlc01
10.31.1.12  hlc02.hlc.eagleworld.net  hlc02.hlc hlc02
10.31.1.13  hlc03.hlc.eagleworld.net  hlc03.hlc hlc03
10.31.1.14  hlc04.hlc.eagleworld.net  hlc04.hlc hlc04
10.31.1.15  hlc05.hlc.eagleworld.net  hlc05.hlc hlc05
10.31.1.16  hlc06.hlc.eagleworld.net  hlc06.hlc hlc06
10.31.1.100 salt.hlc.eagleworld.net   salt.hlc  salt
'''

You can now log on to the Pis with the short username:
$ ssh pi@hlc01

The default password is raspberry.

Temporary Salt Master Setup
---------------------------

To provision the systems, a Salt Master is needed. Set up the Salt Master
using the README instructions located here:

https://gitlab.com/eaglerock/salt-master/blob/master/README.md

In order to get access to the SaltStack configuration used for this
guide, set up the Salt Master's /srv partition to be from this GitLab
repository:

https://gitlab.com/eaglerock/hlc-salt-data

For the server to have the right configuration setup, the master.d
directory from inside this repo should be used.

Once the Docker container is set up, have a shell available inside your
Salt Master container by connecting to it like so:

$ docker exec -it <CONTAINER_NAME> /bin/bash

Raspberry Pi Setup
------------------

TODO: Automate the majority of this away, possibly with the assistance
of the following repo: https://github.com/davidjb/salt-raspberry-pi

Once logged on to the system, you can configure the Raspberry Pi with
the raspi-config command-line utility. The following settings need to be
set:

- Change user password for the pi user
- Under Network Options, set the hostname to the FQDN
  (e.g. hlc01.hlc.eagleworld.net)
- Under Boot Options, enable Wait for Network.
- Under Localisation Options, set the Locale to en_US.UTF-8 UTF-8
- Under Localisation Options, set the Time Zone to America/New_York
- Under Localisation Options, set the WiFi Country to US.
- Under Advanced Options, set the memory split to 16MB for the GPU.

Next update the system:
$ sudo apt update && sudo apt upgrade -y

Update /etc/hosts with the above lines:
$ sudo vi /etc/hosts

Install the Salt Minion on the system:
$ sudo apt install -y salt-minion

Reboot the host
$ sudo reboot

After this, the system should automatically connect to the Salt Master.

Repeat the above steps for all 6 Raspberry Pis.

Salt Provisioning
-----------------

Most of the manual steps are complete at this point. The rest of the
server setup can be done automatically using Salt.

First, connect to the Salt Master:

$ docker run -it <CONTAINER_NAME> /bin/bash

Check to ensure all six Raspberry Pis are connected to the master:

$ salt-key

All 6 Pis should be listed under Unaccepted Keys. Accept them all:

$ salt-key -A

Now, finish the basebuild and set up Kubernetes on the systems:

$ salt '*' state.apply

Initial Cluster Setup
---------------------

TODO: Automate this cluster setup...

Using the file in this repo, run the following on the master node:

$ sudo kubeadm init --config kubeadm-conf.yaml

Once the command completes, save the command listed at the end of the
output for joining the cluster as a worker node.

If you lose the command, you can generate another token and get the
command with the following command:

$ kubeadm token create --print-join-commmand

Next, using the Salt Master, join all of the worker nodes to the cluster:

$ salt -N hlc-workers cmd.run 'sudo <kubeadm_join_command>'

At this point the cluster is built without networking or remote access.

Cluster Local and Remote Access
-------------------------------

In order to manage the master node using the pi user, run the following:

$ mkdir -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config

At this point, all kubectl commands can be run without sudo as the pi user.

In order to enable remote access, a flattened config file is necessary.
Use the following command to generate the file:

$ kubectl config view --flatten=true > /tmp/config

Copy the /tmp/config file to the .kube directory under the intended user's
home directory (e.g. /home/eaglerock/.kube/config).

The remote system can now be used to manage the cluster.

Cluster Networking Setup
------------------------

To enable networking, apply the flannel network on the cluster:

$ kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

To allow connectivity outside of the cluster to the services, an ingress
controller is needed. Traefik can be used to automate the creation of
reverse proxies. 

Create the DaemonSet, Service, ClusterRoleBinding, and Ingress using
the .yaml file in this repo:

$ kubectl apply -f traefik.yaml

Now, all worker nodes in the cluster will respond to inbound requests 
to any of the ingressed services, including the Traefik UI itself.


