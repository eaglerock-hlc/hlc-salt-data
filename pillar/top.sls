base:
  '*':
    - base
    - glusterfs
  'hlc-master':
    - match: nodegroup
    - hlc
    - master
  'hlc-worker':
    - match: nodegroup
    - hlc
    - worker
