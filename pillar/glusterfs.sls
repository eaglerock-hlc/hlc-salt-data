glusterfs:
  server:
    peers:
    - 10.31.1.11
    - 10.31.1.12
    - 10.31.1.13
    - 10.31.1.14
    - 10.31.1.15
    - 10.31.1.16
    volumes:
       ross01:
         storage: /data/glusterfs/hlc01/brick1/brick
         replica: 2
         stripe: 3
         bricks:
         - 10.31.1.11:/data/glusterfs/hlc01/brick1/brick
         - 10.31.1.12:/data/glusterfs/hlc01/brick1/brick
         - 10.31.1.13:/data/glusterfs/hlc01/brick1/brick
         - 10.31.1.14:/data/glusterfs/hlc01/brick1/brick
         - 10.31.1.15:/data/glusterfs/hlc01/brick1/brick
         - 10.31.1.16:/data/glusterfs/hlc01/brick1/brick
         options:
           cluster.readdir-optimize: On
           nfs.disable: On
           network.remote-dio: On
           diagnostics.client-log-level: WARNING
           diagnostics.brick-log-level: WARNING
    enabled: true
  client:
    volumes:
      ross01:
        path: /srv/ross01
        server: localhost
        user: root
        group: root
    enabled: true
