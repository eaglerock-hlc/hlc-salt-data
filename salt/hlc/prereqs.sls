disable swap on the rpis:
  # cmd.run:
  #   - name: 'dphys-swapfile swapoff'
  #   - onlyif:
  #     - pkg.purged: dphys-swapfile
  #       - test: True
  pkg.purged:
    - name: 'dphys-swapfile'

enable cgroup boot parameters:
  file.managed:
    - name: /boot/cmdline.txt
    - source: salt://hlc/prereqs/cmdline.txt
    - user: root
    - group: root
    - mode: 755
