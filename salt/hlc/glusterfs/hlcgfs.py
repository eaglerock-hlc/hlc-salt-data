#!/usr/bin/python3

import os           # OS file manipulation usage
import sys          # System manipulation
import argparse     # Command-line argument parsing
import yaml         # Importing flag list from the conf YAML file
import datetime     # For printing the date in the flagfiles

'''
hlcgfs.py - HLC GlusterFS setup script
12/31/2018 - Peter P. Marks

Peter's "I'm way too damn lazy to figure out how to properly set up a
SaltStack function, so this will get the states to do what I need."
'''

# Editable static variables
script_dir = "/root/scripts"
#script_dir = "."   # Testing setting
conf_file = "hlcgfs.yml"
flag_dir = "/var/log/hlc/flags"
replicas = 3

def import_flags():
    # Import flags from the configuration file
    my_conf = os.path.join(script_dir, conf_file)

    # Error out if the flag file is missing
    if not os.path.exists(my_conf):
        print("ERROR: Missing config file! Please ensure {}".format(conf_file))
        print("       is found in {}.".format(flag_dir))
        sys.exit(1)

    # Open the flag file and load the dictionary
    with open(my_conf, "r") as conf:
        args = dict(yaml.load(conf))

    # Return the dict of arguments
    return args


def get_args():
    # Get Arguments Function = Parses and returns necessary objects
    parser = argparse.ArgumentParser(description="Manage HLC GlusterFS Flags")
    onoff = parser.add_mutually_exclusive_group()
    parser.add_argument('flag', help='The flag to be modified or checked')
    onoff.add_argument('--on', action='store_true')
    onoff.add_argument('--off', action='store_true')
    parser.add_argument('-v', '--verbose', action='store_true')
    return parser.parse_args()


def turn_on(my_flag, verbosity):
    # Turn on the flag in question by creating the flagfile
    my_line = datetime.datetime.now().isoformat()
    if verbosity:
        if os.path.exists(my_flag):
            print("File {} is already present. Overwriting...".format(my_flag))
        else:
            print("Creating {}...".format(my_flag))
    with open(my_flag, "w+") as my_flagfile:
        my_flagfile.write("{}\n".format(my_line))


def turn_off(my_flag, verbosity):
    # Turn off the flag in question by deleting the flagfile
    if os.path.exists(my_flag):
        if verbosity:
            print("Deleting {}...".format(my_flag))
        os.remove(my_flag)
    elif verbosity:
            print("File {} is not present.".format(my_flag))


def get_status(my_flag, my_path, verbosity):
    # Print the status of the flag in question
    status = os.path.exists(my_path)
    if verbosity:
        if status:
            print("Flag '{}' is on. File {} is present.".format(my_flag, my_path))
        else:
            print("Flag '{}' is off. File {} not found.".format(my_flag, my_path))
    else:
        print(status)


def main():
    # Main Function
    my_flags = import_flags()
    my_args = get_args()
    my_filename = my_args.flag + ".flag"
    my_path = os.path.join(flag_dir, my_filename)

    # Check if the input flag is in the list and error out if not present
    if my_args.flag not in list(my_flags.keys()):
        if my_args.verbose:
            print("Error: '{}' is not a valid flag.".format(my_args.flag))
            print("Available flags:")
            for flag in my_flags.keys():
                print("    {}: {}".format(flag, my_flags[flag]["description"]))
        sys.exit(1)
    elif my_args.on:
        # Turn on the flag
        turn_on(my_path, my_args.verbose)
    elif my_args.off:
        # Turn off the flag
        turn_off(my_path, my_args.verbose)
    else:
        # No flag was selected, so the status is printed out
        get_status(my_args.flag, my_path, my_args.verbose)

# Entry point
if __name__ == "__main__":
    main()
