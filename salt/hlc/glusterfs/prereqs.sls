install glusterfs:
  pkg.installed:
    - pkgs:
      - glusterfs-server
      - glusterfs-client

enable glusterfs server:
  service.running:
    - name: glusterfs-server
    - enable: True

install PyYAML:
  pip.installed:
    - name: PyYAML
    - bin_env: '/usr/bin/pip3'

copy hlcgfs Python script:
  file.managed:
    - name: /root/scripts/hlcgfs.py
    - source: salt://hlc/glusterfs/hlcgfs.py
    - user: root
    - group: root
    - mode: 700
    - makedirs: True

copy hlcgfs Python yaml file:
  file.managed:
    - name: /root/scripts/hlcgfs.yml
    - source: salt://hlc/glusterfs/hlcgfs.yml
    - user: root
    - group: root
    - mode: 600

create hlcgfs flag directory:
  file.directory:
    - name: /var/log/hlc/flags
    - user: root
    - group: root
    - makedirs: True
