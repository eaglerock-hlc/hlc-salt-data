format.flag:
  file.missing:
    - name: /var/log/hlc/flags/format.flag

format the the /dev/mmcblk0p3 partition:
  blockdev.formatted:
    - name: /dev/mmcblk0p3
    - fs_type: xfs
    - force: True
    - require:
      - file: format.flag

toggle format flag:
  cmd.run:
    - name: /root/scripts/hlcgfs.py format --on
    - onchanges:
      - blockdev: /dev/mmcblk0p3

mount the new filesystem and add to fstab:
  mount.mounted:
    - name: /data/brick1
    - device: /dev/mmcblk0p3
    - fstype: xfs
    - pass_num: 2
    - mkmnt: True
    - opts:
      - defaults

create gluster volume directory:
  file.directory:
    - name: /data/brick1/ross01
    - user: root
    - group: root
    - mode: 700

create the glusterfs mount directory:
  file.directory:
    - name: /srv/ross01/
    - user: root
    - group: root
    - mode: 700
    - makedirs: True
