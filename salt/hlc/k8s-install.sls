{% if not grains['docker.installed'] is defined %}
install Docker and toggle installed grain:
  cmd.script:
    - name: https://get.docker.com/
  grains.present:
    - name: docker.installed
    - value: True
{% endif %}

enable docker for the pi user:
  group.present:
    - name: docker
    - addusers:
      - pi

set up the Kubernetes repo:
  pkgrepo.managed:
    - humanname: Kubernetes Repo
    - name: deb http://apt.kubernetes.io/ kubernetes-xenial main
    - file: /etc/apt/sources.list.d/kubernetes.list
    - gpg_check: 1
    - key_url: https://packages.cloud.google.com/apt/doc/apt-key.gpg

install the kubeadm binary:
  pkg.installed:
    - pkgs:
      - kubeadm
