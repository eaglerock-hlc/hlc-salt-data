base:
  '*':
    - basebuild
    - hlc
  'hlc-master':
    - match: nodegroup
    - master.pkg
    - hlc.glusterfs.master
  'hlc-worker':
    - match: nodegroup
    - hlc.glusterfs.worker
