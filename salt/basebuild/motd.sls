copy motd files to /etc:
  file.recurse:
    - name: /etc
    - source: salt://basebuild/motd/etc
    - user: root
    - group: root
    - file_mode: 644

copy update-motd files to /etc/update-motd.d and purge extra files:
  file.recurse:
    - name: /etc/update-motd.d
    - source: salt://basebuild/motd/update-motd.d
    - user: root
    - group: root
    - file_mode: 755
    - clean: True
