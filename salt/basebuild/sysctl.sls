disable ipv6:
  file.append:
    - name: /etc/sysctl.conf
    - text: net.ipv6.conf.all.disable_ipv6 = 1
