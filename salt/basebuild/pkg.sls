ensure the system is up to date:
  pkg.uptodate:
    - refresh: True

install basic CLI utilities:
  pkg.installed:
    - pkgs:
      - vim
      - htop
      - glances
      - net-tools
      - rsync
      - lshw
      - lsof
      - nmap
      - xfsprogs
      - python-pip
      - python3-pip

uninstall usbmount to avoid USB mount conflicts:
  pkg.purged:
    - name: usbmount
