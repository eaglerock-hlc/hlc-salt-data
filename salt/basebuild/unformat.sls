# format the the /dev/mmcblk0p3 partition:
#   blockdev.formatted:
#     - name: /dev/mmcblk0p3
#     - fs_type: ext4

unmount initial glusterfs partition:
  mount.unmounted:
    - name: /data/glusterfs/hlc01/brick1
    - device: /dev/mmcblk0p3
    - persist: True

purge brick directory and parent directories:
  file.absent:
    - name: /data/

purge former mount point:
  file.absent:
    - name: /srv/ross01/
