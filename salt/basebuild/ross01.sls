mount the ross01 glusterfs filesystem and add to fstab:
  mount.mounted:
    - name: /srv/ross01
    - device: localhost:/ross01
    - fstype: glusterfs
    - pass_num: 2
    - mkmnt: True
    - opts:
      - defaults
    - require:
      - file: 
